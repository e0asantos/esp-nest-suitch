# change the name of the program to
# compile one of the versions
PROGRAM = esp-nest-temp

SPIFFS_BASE_ADDR = 0x200000
SPIFFS_SIZE = 0x100000
FLASH_SIZE = 32
EXTRA_COMPONENTS = \
	extras/http-parser \
	extras/dhcpserver \
	extras/spiffs \
	$(abspath ./esp-ir) \
	$(abspath ./esp-cjson) \
	$(abspath ./esp-wifi-config) \
	$(abspath ./esp-button)

BUTTON_GPIO ?= 0
LIBS += m

EXTRA_CFLAGS += -I../.. -DBUTTON_GPIO=$(BUTTON_GPIO) -w

include $(SDK_PATH)/common.mk



monitor:
	$(FILTEROUTPUT) --port $(ESPPORT) --baud 115200 --elf $(PROGRAM_OUT) 

# EXTRA_COMPONENTS = \
# 	extras/http-parser \
# 	$(abspath ../../components/common/wolfssl) \