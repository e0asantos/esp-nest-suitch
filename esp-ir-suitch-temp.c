#include <stdio.h>
#include <stdlib.h>
#include <esp/uart.h>
#include <FreeRTOS.h>
#include <task.h>

#include <ir/ir.h>
#include <ir/raw.h>
#include <button.h>
#include <sysparam.h>

#include "wifi_config.h"
#include <math.h>

#include "espressif/esp_common.h"

#include <unistd.h>
#include <string.h>
#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"

#include "fcntl.h"
#include "spiffs.h"
#include "esp_spiffs.h"


#ifndef BUTTON_GPIO
#error BUTTON_GPIO just be defined
#endif

static int IR_RX_GPIO = 5;
static int RSSI_TEST = 0;
bool isRecording = false;

// socket for the suitch connection
int s = NULL;
// variable to control de number of times 3 seconds where pressed
int longPresses = 0;
int commandSemaphore = 0;
// variable referencing the token string
char *suitchToken = NULL;



static int led_gpio = 2;
bool led_on = false;

float temperature_read = 16;

void sendStoredCommand(){
  int16_t command_local[360] = {};
  uint16_t buffer_size = sizeof(int16_t) * 1024;
  int read_bytes = 0;
  spiffs_file fd = SPIFFS_open(&fs, "one.suitch", SPIFFS_RDONLY, 0);
  if (fd < 0) {
      printf("Error opening file_R\n");
  } else {
    read_bytes = SPIFFS_read(&fs, fd, command_local, buffer_size);
    // it is devided by 2 because is a 16 bit number, thus needs twice the space
    for (int i = 0; i < read_bytes/2; i++)
    {
      printf("%5d ", command_local[i]);
      if (i % 16 == 15)
        printf("\n");
    }
    // free(indexToRead);
    printf("Found command A_task( length = %d)\n", read_bytes/2);
    SPIFFS_close(&fs, fd);
    printf("_closed");
  }
  led_write(true);
  vTaskDelay(100 / portTICK_PERIOD_MS);
  led_write(false);
  printf("sendStoredCommand()\n");
  ir_tx_init();
  ir_raw_send(command_local, read_bytes/2);
  vTaskDelete(NULL);
}
void sendStoredSecondCommand(){
  int16_t command_local[360] = {};
  uint16_t buffer_size = sizeof(int16_t) * 1024;
  int read_bytes = 0;
  spiffs_file fd = SPIFFS_open(&fs, "two.suitch", SPIFFS_RDONLY, 0);
  if (fd < 0) {
      printf("Error opening file_R\n");
  } else {
    read_bytes = SPIFFS_read(&fs, fd, command_local, buffer_size);
    // it is devided by 2 because is a 16 bit number, thus needs twice the space
    for (int i = 0; i < read_bytes/2; i++)
    {
      printf("%5d ", command_local[i]);
      if (i % 16 == 15)
        printf("\n");
    }
    // free(indexToRead);
    printf("Found command B_task( length = %d)\n", read_bytes/2);
    SPIFFS_close(&fs, fd);
  }
  led_write(true);
  vTaskDelay(100 / portTICK_PERIOD_MS);
  led_write(false);
  printf("sendStoredSecondCommand()\n");
  ir_tx_init();
  ir_raw_send(command_local, read_bytes/2);
  vTaskDelete(NULL);
}

/* void loadStoredCommand_task(void *_args){
  memset(&command1[0], 0, sizeof(command1));
  uint16_t buffer_size = sizeof(int16_t) * 1024;

  spiffs_file fd = SPIFFS_open(&fs, "one.suitch", SPIFFS_RDONLY, 0);
  if (fd < 0) {
      printf("Error opening file_R\n");
  } else {
    printf("sizeof command1:%d\n", sizeof(command1));
    int read_bytes = SPIFFS_read(&fs, fd, command1, buffer_size);
    currentSize = read_bytes/2;
    printf("Read %d bytes, sizeof:%d\n", read_bytes, sizeof(command1));

    for (int i = 0; i < currentSize; i++)
    {
      printf("%5d ", command1[i]);
      if (i % 16 == 15)
        printf("\n");
    }
    // free(indexToRead);
    printf("Found command A_task( length = %d)\n", currentSize);
    SPIFFS_close(&fs, fd);
  }
    // buf[read_bytes] = '\0';    // zero terminate string
    // printf("Data: %s\n", buf);

  vTaskDelete(NULL);
}
void resetCommands_task(){
  int oneRecordRead = NULL;
  char indexToRead[5];
  char indexToReadb[5];
  for (int i = 0; i < 300; i++)
  {
    snprintf(indexToRead, 5, "b%d", i);
    snprintf(indexToReadb, 5, "a%d", i);
    sysparam_set_int32(indexToRead, NULL);
    sysparam_set_int32(indexToReadb, NULL);

  }
  vTaskDelete(NULL);
}
void loadStoredSecondCommand_task(void *_args){
  memset(&command2[0], 0, sizeof(command2));
  int oneRecordRead = NULL;
  if(sysparam_get_int32("b0", (int *)&oneRecordRead) == SYSPARAM_OK){
    char indexToRead[5];
    for (int i = 0; i < 360; i++)
    {
      snprintf(indexToRead, 5, "b%d", i);
      if(sysparam_get_int32(indexToRead, (int *)&oneRecordRead) == SYSPARAM_OK){
        command2[i] = oneRecordRead;
      }  else {
        secondCommandSize = i + 1;
        break;
      }
    }
    // free(indexToRead);
    printf("Found command B_task( length = %d )\n", secondCommandSize);
  }
  // free(oneRecordRead);
  vTaskDelete(NULL);
} */

void led_write(bool on) {
  gpio_write(led_gpio, on ? 0 : 1);
  
}

void pins_init() {
  // led GPIO 2 is the status indicator
  gpio_enable(led_gpio, GPIO_OUTPUT);
  led_write(led_on);
  // BUTTON
  // BUTTON_GPIO ?= 0
  // receiver for IRCodes
  // static int IR_RX_GPIO = 5;
  // transmitter for IRCodes = GPIO14
  // NEST1
  gpio_enable(16, GPIO_INPUT);
  // NEST2
  gpio_enable(9, GPIO_INPUT);
  // NEST3
  gpio_enable(12, GPIO_INPUT);
  // NEST2
  gpio_enable(13, GPIO_INPUT);
  // NEST2
  gpio_enable(4, GPIO_INPUT);
  // NEST2
  gpio_enable(10, GPIO_INPUT);

}



void get_raw_temperature(){
    static float A = 0.001129148;   // thermistor equation parameters
    static float B = 0.000234125;
    static float C = 0.0000000876741; 
    float Vout, Rth, temperature_adc, adc_value; 

    adc_value = sdk_system_adc_read();
    Vout = (adc_value * 3.3) / 1023;
    // We use the constant 1.7 for Resistor 10k and thermistor 100k and VCC = 3.3
    // We use the constant 6 for Resistor 1k and thermistor 10k and VCC = 3.3
    // We use the constant .7 for Resistor 10k and thermistor 100k and VCC = 5
    // We use the constant 2.5 for Resistor 100k and thermistor 100k and VCC = 3.3 (wemos)
    Rth = ((3.3 * 10000 / Vout) - 10000);
    printf("ADC start  \n");
    temperature_adc = (1 / (A + (B * log(Rth)) + (C * pow((log(Rth)),3))));   // Temperature in kelvin
    
    temperature_adc = (temperature_adc - 273.15);  // Temperature in degree celsius
    printf("ADC read [%f] Temp [%f] Rth [%f]\n", adc_value , temperature_adc, Rth);
    // if( temperature_adc < 0){
    //   temperature_adc = temperature_adc * -1;
    // }
    // temperature_adc = temperature_adc + 9.5;
    printf("ADC correct [%f] Temp [%f] Rth [%f]\n", adc_value , temperature_adc, Rth);
    temperature_read = temperature_adc;
    // free(Vout);
    // free(Rth);
    // free(temperature_adc);
    // free(adc_value);
}

void get_temperature_task(void *_args){
    get_raw_temperature();
    vTaskDelete(NULL);
}

void led_identify_task(void *_args) {
    for (int i=0; i<3; i++) {
        for (int j=0; j<2; j++) {
            led_write(true);
            vTaskDelay(50 / portTICK_PERIOD_MS);
            led_write(false);
            vTaskDelay(50 / portTICK_PERIOD_MS);
            led_write(true);
        }

        vTaskDelay(250 / portTICK_PERIOD_MS);
    }

    led_write(led_on);

    vTaskDelete(NULL);
}

void led_identify() {
    printf("LED identify\n");
    xTaskCreate(led_identify_task, "LED identify", 128, NULL, 2, NULL);
}



void get_my_id(void) {
    // Use MAC address for Station as unique ID
    uint8_t macaddr[6];
    sdk_wifi_get_macaddr(STATION_IF, macaddr);
    int name_len = snprintf(NULL, 0, "cws%02X%02X%02X", macaddr[3], macaddr[4], macaddr[5]);
    char *name_value = malloc(name_len + 1);
    snprintf(name_value, name_len + 1, "cws%02X%02X%02X", macaddr[3], macaddr[4], macaddr[5]);
    printf(name_value);
    // free(macaddr);
    // free(name_len);
    // free(name_value);
}


void ir_dump_task(void *arg)
{
  ir_rx_init(IR_RX_GPIO, 1024);
  ir_decoder_t *raw_decoder = ir_raw_make_decoder();
  uint16_t buffer_size = sizeof(int16_t) * 1024;
  int16_t *buffer = malloc(buffer_size);
  while (isRecording)
  {
    if (isRecording && longPresses == 1)
    { 
      printf("START A:\n");  
      int size = ir_recv(raw_decoder, 0, buffer, buffer_size);
      printf("PRE (size = %d):\n", size);
      if (size <= 20)
        continue;
      // currentSize = size;
      printf("Decoded packet (size = %d):\n", size);
      for (int i = 0; i < size; i++)
      {
        printf("%5d ", buffer[i]);
        if (i % 16 == 15)
          printf("\n");
      }
      int fd = open("one.suitch", O_WRONLY|O_CREAT, 0);
      if (fd < 0) {
          printf("Error opening one file_W\n");
      } else {
        int written = write(fd, buffer, size*2);
        printf("Written %d bytes\n", written);
        printf("size:%d, sizeof:%d, original size:%d\n",size, sizeof(buffer), buffer_size);
        close(fd);
      }
      if (size % 16)
        printf("\n");
      isRecording = false;
      longPresses = 0;
      
    } else if(isRecording && longPresses == 2){
      printf("START B:\n");  
      // memset(&command2[0], 0, sizeof(command2));
      int size = ir_recv(raw_decoder, 0, buffer, buffer_size);
      printf("PRE (size = %d):\n", size);
      if (size <= 20)
        continue;
      // secondCommandSize = size;
      printf("Decoded packet (size = %d):\n", size);
      int fd = open("two.suitch", O_WRONLY|O_CREAT, 0);
      if (fd < 0) {
          printf("Error opening two file_W\n");
      } else {
        int written = write(fd, buffer, size*2);
        printf("Written %d bytes\n", written);
        close(fd);
      }
      if (size % 16)
        printf("\n");
      
      isRecording = false;
      longPresses = 0;
      
    }
  }
  raw_decoder->free(raw_decoder);
  xTaskCreate(led_identify_task, "LED identify", 128, NULL, 2, NULL);
  vTaskDelete(NULL);
}

void test_voltage(void *arg)
{
  gpio_enable(14, GPIO_OUTPUT);
  gpio_write(14,1);
  vTaskDelay(1500 / portTICK_PERIOD_MS);
  gpio_enable(14, GPIO_OUTPUT);
  gpio_write(14,0);
  vTaskDelete(NULL);
}



void button_callback(button_event_t event, void *context)
{
  switch (event)
  {
  case button_event_single_press:
    // xTaskCreate(loadStoredCommand_task, "loadConfiguration", 2048, NULL, tskIDLE_PRIORITY, NULL);
    // xTaskCreate(get_temperature_task, "Temperature reading", 256, NULL, 2, NULL);
    if( commandSemaphore == 0){
       xTaskCreate(sendStoredCommand, "sendIR", 2048, NULL, 2, NULL);
       //xTaskCreate(test_voltage, "LED identify", 256, NULL, 2, NULL);
      commandSemaphore = 1;
     } else {
       xTaskCreate(sendStoredSecondCommand, "sendIR2", 2048, NULL, 2, NULL);
       commandSemaphore = 0;
       // xTaskCreate(test_voltage, "LED identify", 256, NULL, 2, NULL);
     }
    break;
  case button_event_double_press:
    printf("double press\n");
    break;
  case button_event_long_press:
    printf("long press begin\n");
    // let's see if the recording step was previously invoked, if it was, then
    // the user wants to erase the wifi configs
    longPresses = longPresses + 1;
    // if (longPresses>2)
    // {
    //   // xTaskCreate(resetCommands_task, "Reset commands", 128, NULL, 2, NULL);
    //   wifi_config_reset();
    //   vTaskDelay(1000 / portTICK_PERIOD_MS);
    //   printf("Restarting\n");
    //   sdk_system_restart();
    // }
    // xTaskCreate(led_identify_task, "LED identify", 128, NULL, 2, NULL);
    // // show blinking
    // if (longPresses == 1)
    // {
    
    isRecording = true;
    // ir_rx_init(IR_RX_GPIO, 1024);
    xTaskCreate(ir_dump_task, "IR dump", 2048, NULL, tskIDLE_PRIORITY, NULL);
    printf("long press end\n");
    // break;
      /* code */
    // }
    
  }
}

#define WEB_SERVER "suitch.network"
#define WEB_PORT "3000"
#define WEB_PATH ""

void http_get_task(void *pvParameters)
{
    int successes = 0, failures = 0;
    printf("HTTP get task starting...\r\n");

    while(1) {
        const struct addrinfo hints = {
            .ai_family = AF_UNSPEC,
            .ai_socktype = SOCK_STREAM,
        };
        struct addrinfo *res;

        // printf("Running DNS lookup for %s...\r\n", WEB_SERVER);
        int err = getaddrinfo(WEB_SERVER, WEB_PORT, &hints, &res);

        if (err != 0 || res == NULL) {
            // printf("DNS lookup failed err=%d res=%p\r\n", err, res);
            if(res)
                freeaddrinfo(res);
            vTaskDelay(1000 / portTICK_PERIOD_MS);
            failures++;
            continue;
        }

#if LWIP_IPV6
        {
            struct netif *netif = sdk_system_get_netif(0);
            int i;
            for (i = 0; i < LWIP_IPV6_NUM_ADDRESSES; i++) {
                printf("  ip6 %d state %x\n", i, netif_ip6_addr_state(netif, i));
                if (!ip6_addr_isinvalid(netif_ip6_addr_state(netif, i)))
                    printf("  ip6 addr %d = %s\n", i, ip6addr_ntoa(netif_ip6_addr(netif, i)));
            }
        }
#endif

        struct sockaddr *sa = res->ai_addr;
        if (sa->sa_family == AF_INET) {
            printf("DNS lookup succeeded. IP=%s\r\n", inet_ntoa(((struct sockaddr_in *)sa)->sin_addr));
        }
#if LWIP_IPV6
        if (sa->sa_family == AF_INET6) {
            printf("DNS lookup succeeded. IP=%s\r\n", inet6_ntoa(((struct sockaddr_in6 *)sa)->sin6_addr));
        }
#endif

        s = socket(res->ai_family, res->ai_socktype, 0);
        if(s < 0) {
            printf("... Failed to allocate socket.\r\n");
            freeaddrinfo(res);
            vTaskDelay(1000 / portTICK_PERIOD_MS);
            failures++;
            continue;
        }

        printf("... allocated socket\r\n");

        if(connect(s, res->ai_addr, res->ai_addrlen) != 0) {
            close(s);
            freeaddrinfo(res);
            printf("... socket connect failed.\r\n");
            vTaskDelay(4000 / portTICK_PERIOD_MS);
            failures++;
            continue;
        }

        

        printf("... connected\r\n");
        freeaddrinfo(res);

        
        char *new_str;
        if( suitchToken != NULL){
          if((new_str = malloc(strlen(&suitchToken)+strlen("\r\n")+1)) != NULL){
            printf("...token\r\n");
              new_str[0] = '\0';   // ensures the memory is an empty string
              strcat(new_str,suitchToken);
              strcat(new_str,"\r\n");
          } 
        }
        char *source_str;
        char *ssid;
        if( suitchToken != NULL){
          sysparam_get_string("wifi_ssid", &ssid);
          if((source_str = malloc(strlen(&ssid)+strlen("\r\n")+1)) != NULL){
              printf("...source\r\n");
              source_str[0] = '\0';   // ensures the memory is an empty string
              strcat(source_str,"hotspot ");
              strcat(source_str,ssid);
              strcat(source_str,"\r\n");
          } 
        }

        char *req;
        if (suitchToken == NULL){
          req = "esp ipsum dolor sit amet\r\n";
        } else {
          req = new_str;
        }
        if (write(s, req, strlen(req)) < 0) {
            printf("... socket send failed\r\n");
            close(s);
            vTaskDelay(4000 / portTICK_PERIOD_MS);
            failures++;
            continue;
        }
        // free (new_str);
        // free(req);
        // free(ssid);
        printf("... socket send success\r\n");

        static char recv_buf[128];
        int r;
        do {
            bzero(recv_buf, 128);
            r = read(s, recv_buf, 127);
            if(r > 0) {
                printf("%s", recv_buf);
                if(suitchToken == NULL && strstr( recv_buf, "TOKEN?" ) == NULL && strstr( recv_buf, "WLCME" ) == NULL && strstr( recv_buf, "WLCMEX" ) == NULL ){
                  suitchToken = &recv_buf[0];
                  printf("suitchToken:%s", suitchToken);
                  sysparam_set_string("token3", suitchToken);
                } else if( strstr( recv_buf, "off" ) != NULL){
                  xTaskCreate(sendStoredSecondCommand, "sendIR2", 2048, NULL, 2, NULL);
                } else if( strstr( recv_buf, "off" ) != NULL || strstr( recv_buf, "on" ) != NULL){
                  xTaskCreate(sendStoredCommand, "sendIR", 2048, NULL, 2, NULL);
                } else if(suitchToken != NULL && (strstr( recv_buf, "WLCME" ) != NULL || strstr( recv_buf, "WLCMEX" ) != NULL)){
                  if (write(s, source_str, strlen(source_str)) < 0) {
                
                  }
                  // free memory
                  // free(source_str);
                } else if(suitchToken != NULL && (strstr( recv_buf, "C1" ) != NULL || strstr( recv_buf, "c1" ) != NULL)){
                  xTaskCreate(led_identify_task, "LED identify", 128, NULL, 2, NULL);
                  longPresses = 1;
                  isRecording = true;
                  xTaskCreate(ir_dump_task, "IR dump", 2048, NULL, tskIDLE_PRIORITY, NULL);
                } else if(suitchToken != NULL && (strstr( recv_buf, "C2" ) != NULL || strstr( recv_buf, "c2" ) != NULL)){
                  xTaskCreate(led_identify_task, "LED identify", 128, NULL, 2, NULL);
                  longPresses = 2;
                  isRecording = true;
                  xTaskCreate(ir_dump_task, "IR dump", 2048, NULL, tskIDLE_PRIORITY, NULL);
                }
            }
            // taskYIELD();
        } while(r > 0);

        printf("... done reading from socket. Last read return=%d errno=%d\r\n", r, errno);
        if(r != 0)
            failures++;
        else
            successes++;
        close(s);
        s = NULL;
        printf("successes = %d failures = %d\r\n", successes, failures);
        for(int countdown = 10; countdown >= 0; countdown--) {
            printf("%d... ", countdown);
            vTaskDelay(1000 / portTICK_PERIOD_MS);
        }
        printf("\r\nStarting again!\r\n");
    }
}
void beat_task( void * pvParameters )
 {
 TickType_t xLastWakeTime;

     // Initialise the xLastWakeTime variable with the current time.
     xLastWakeTime = xTaskGetTickCount();
     
     if (sysparam_get_string("token3", &suitchToken) != SYSPARAM_OK) {
        suitchToken = NULL;
     }
     for( ;; )
     {
         // Wait for the next cycle.
         vTaskDelayUntil( &xLastWakeTime, 45000 / portTICK_PERIOD_MS );
         xTaskCreate(get_temperature_task, "Temperature reading", 256, NULL, 2, NULL);
         
         if (s != NULL && suitchToken != NULL){
            const char *req ="ping\r\n";
            if (write(s, req, strlen(req)) < 0) {
                
            }
            char output[6];
            snprintf(output, 6, "%f", temperature_read);
            char *new_str;
            if((new_str = malloc(strlen("temp=")+strlen((char*)&output)+1)) != NULL){
              new_str[0] = '\0';   // ensures the memory is an empty string
              strcat(new_str,"temp=");
              strcat(new_str,(char*)&output);
              strcat(new_str,"\r\n");
            }
            if (write(s, new_str, strlen(new_str)) < 0) {
                
            }
            // free(output);
            // free(new_str);
            // save the temperature reading
         } else if(s != NULL && suitchToken == NULL){
         }
     }
 }
 void pin_read_task( void * pvParameters )
 {
 TickType_t xLastWakeTime;

     // Initialise the xLastWakeTime variable with the current time.
     xLastWakeTime = xTaskGetTickCount();

     for( ;; )
     {
         // Wait for the next cycle.
         vTaskDelayUntil( &xLastWakeTime, 3000 / portTICK_PERIOD_MS );
         printf("Reading nest1:%d  nest2:%d  nest3:%d  nest4:%d  nest5:%d  nest6:%d\n", 
         gpio_read(16),gpio_read(9),gpio_read(12),gpio_read(13),gpio_read(4), gpio_read(10));
     }
 }
void on_wifi_event(wifi_config_event_t event) {
    if (event == WIFI_CONFIG_CONNECTED) {
        printf("Connected to WiFi\n");
        // get serial number
        get_my_id();
        // get real temperature value
        get_raw_temperature();
    } else if (event == WIFI_CONFIG_DISCONNECTED) {
        printf("Disconnected from WiFi\n");
    }
}

void user_init(void)
{
  esp_spiffs_init();   // allocate memory buffers
  if (esp_spiffs_mount() != SPIFFS_OK) {
      if (SPIFFS_format(&fs) == SPIFFS_OK) {
        printf("Format complete_1\n");
      } else {
        printf("Format failed_1\n");
      }
      if (esp_spiffs_mount() != SPIFFS_OK) {
        printf("Error mount SPIFFS_2\n");
      }
  } else {
    printf("SPIFFS mounted!_1\n");
  }
  uart_set_baud(0, 115200);
  if (sysparam_get_string("token3", &suitchToken) != SYSPARAM_OK) {
    // first thing to get
    suitchToken = NULL;
  }
  printf("==============INIT===============\n");
  printf("==============INIT===============\n");
  printf("==============INIT===============\n");
  pins_init();
  button_config_t button_config = BUTTON_CONFIG(
          .active_level = button_active_low, );
  int program_btn = button_create(BUTTON_GPIO, button_config, button_callback, NULL);
  if (program_btn)
  {
    printf("Failed to initalize PROGRAM button (code %d)\n", program_btn);
  }
  if(RSSI_TEST == 1){
    sysparam_set_string("wifi_ssid", "Unknown");
    sysparam_set_string("wifi_password", "daredevilme");
  }
  // wifi_config_init2("suitch-IR", NULL, on_wifi_event);
  // xTaskCreate(loadStoredCommand_task, "loadConfiguration", 2048, NULL, tskIDLE_PRIORITY, NULL);
  // xTaskCreate(loadStoredSecondCommand_task, "loadConfiguration", 2048, NULL, tskIDLE_PRIORITY, NULL);
  xTaskCreate(&pin_read_task, "beat_task", 384, NULL, 1, NULL);
  // {{apiazure.host}}/v2/mm/myproducts?fetch=10&legalEntityId=99010.1&page=1&salesDocumentTypeId=1&hasDocument=true xTaskCreate(&http_get_task, "get_task", 384, NULL, 2, NULL);
  // enables this PIN in the suitch hardware to drive the mosfet
  // enables GPIO2 as a status indicator
}